const checkuser = resolve => {
    require.ensure(['./components/dashMenuMod.vue'], () => {
        resolve(require('./components/dashMenuMod.vue'));
    })
}

const logueos = resolve => {
    require.ensure(['./components/login.vue'], () => {
        resolve(require('./components/login.vue'));
    })
}

const inicio = resolve => {
    require.ensure(['./components/dashHome.vue'],()=>{
        resolve(require('./components/dashHome.vue'));
    })
}

const presupuestal = resolve => {
    require.ensure(['./components/dashPresupuestal.vue'], () => {
        resolve(require('./components/dashPresupuestal.vue'))
    })
}

const facturacion = resolve => {
    require.ensure(['./components/dashfacturaciones.vue'], () => {
        resolve(require('./components/dashfacturaciones.vue'))
    })
}

const nominas = resolve => {
    require.ensure(['./components/dashNominas.vue'], () => {
        resolve(require('./components/dashNominas.vue'))
    })
}

const cobranzas = resolve => {
    require.ensure(['./components/dashCobranzas.vue'], () => {
        resolve(require('./components/dashCobranzas.vue'))
    })
}

const impuestos = resolve => {
    require.ensure(['./components/dashImpuestos.vue'], () => {
        resolve(require('./components/dashImpuestos.vue'))
    })
}

const recepcion = resolve => {
    require.ensure(['./components/dashImpuestos.vue'], () => {
        resolve(require('./components/dashImpuestos.vue'))
    })
}

const auditoria = resolve => {
    require.ensure(['./components/dashAuditorias.vue'], () => {
        resolve(require('./components/dashAuditorias.vue'))
    })
}

const sucursal = resolve => {
    require.ensure(['./components/dashSucursales.vue'], () => {
        resolve(require('./components/dashSucursales.vue'))
    })
}

const rechuman = resolve => {
    require.ensure(['./components/dashRecHuman.vue'], () => {
        resolve(require('./components/dashRecHuman.vue'))
    })
}

const ctrlpatri = resolve => {
    require.ensure(['./components/dashContPat.vue'], () => {
        resolve(require('./components/dashContPat.vue'))
    })
}

const ventas = resolve => {
    require.ensure(['./components/dashVentas.vue'], () => {
        resolve(require('./components/dashVentas.vue'))
    })
}

const rentas = resolve => {
    require.ensure(['./components/dashRentas.vue'], () => {
        resolve(require('./components/dashRentas.vue'))
    })
}

const c4base = resolve => {
    require.ensure(['./components/dashC4Base.vue'], () => {
        resolve(require('./components/dashC4Base.vue'))
    })
}
// dashboard de mantenimiento   --->
const mantenimiento = resolve => {
    require.ensure(['./components/dashMantto.vue'], () => {
        resolve(require('./components/dashMantto.vue'))
    })
}

const modEmpresas = resolve => {
    require.ensure(['./components/modulos/confEmpresas.vue'], () => {
        resolve(require('./components/modulos/confEmpresas.vue'))
    })
}

const modUnidades = resolve => {
    require.ensure(['./components/modulos/confUnidades.vue'], () => {
        resolve(require('./components/modulos/confUnidades.vue'))
    })
}

const modPuestos = resolve => {
    require.ensure(['./components/modulos/confPuestos.vue'], () => {
        resolve(require('./components/modulos/confPuestos.vue'))
    })
}

const modBancos = resolve => {
    require.ensure(['./components/modulos/confBancos.vue'], () => {
        resolve(require('./components/modulos/confBancos.vue'))
    })
}

const modCtasMadre = resolve => {
    require.ensure(['./components/modulos/confCtasMadre.vue'], () => {
        resolve(require('./components/modulos/confCtasMadre.vue'))
    })
}

const modSubCuentas = resolve => {
    require.ensure(['./components/modulos/confSubCtas.vue'], () => {
        resolve(require('./components/modulos/confSubCtas.vue'))
    })
}

const modSucursales = resolve => {
    require.ensure(['./components/modulos/confSucursales.vue'], () => {
        resolve(require('./components/modulos/confSucursales.vue'))
    })
}

const modReembolsables = resolve => {
    require.ensure(['./components/modulos/confReembolsables.vue'], () => {
        resolve(require('./components/modulos/confReembolsables.vue'))
    })
}

const modCatProdServ = resolve => {
    require.ensure(['./components/modulos/confCatProdServ.vue'], () => {
        resolve(require('./components/modulos/confCatProdServ.vue'))
    })
}

const catClientes = resolve => {
    require.ensure(['./components/presupuestal/catClientes.vue'], () => {
        resolve(require('./components/presupuestal/catClientes.vue'))
    })
}

// --------------------------   --->

// Código Base para cargar rutas   --->
// const mod = resolve => {
//     require.ensure(['./components/modulos'], () => {
//         resolve(require('./components/modulos'))
//     })
// }
// -----------------------------   --->

export const rutas = [{path: '/siafi/app', component: checkuser, name: 'inicio'},
    {path: '/siafi/app/login', component: logueos, name: 'login'},
    {path: '/siafi/app/inicio', component: inicio, name: 'home', children: [
        {path: 'presupuestal', component: presupuestal, name: 'presupuestal'},
        {path: 'facturacion', component: facturacion, name: 'facturacion', children: [
            {path: 'catClientes', component: catClientes, name: 'factCatCtes' }
        ]},
        {path: 'nominas', component: nominas, name: 'nominas'},
        {path: 'cobranza', component: cobranzas, name: 'cobranza'},
        {path: 'impuestos', component: impuestos, name: 'impuestos'},
        {path: 'recepcion', component: recepcion, name: 'recepcion'},
        {path: 'auditoria', component: auditoria, name: 'auditoria'},
        {path: 'sucursal', component: sucursal, name: 'sucursal'},
        {path: 'rechuman', component: rechuman, name: 'recHuman'},
        {path: 'ctrlpatri', component: ctrlpatri, name: 'ctrlpatri'},
        {path: 'ventas', component: ventas, name: 'ventas'},
        {path: 'rentas', component: rentas, name: 'rentas'},
        {path: 'c4base', component: c4base, name: 'c4base'},
        {path: 'mantenimiento', component: mantenimiento, name: 'mantenimiento', children: [
            {path: 'modEmpresas', component: modEmpresas, name: 'manttoEmpresas'},
            {path: 'modUnidades', component: modUnidades, name: 'manttoUnidades' },
            {path: 'modPuestos', component: modPuestos, name: 'manttoPuestos' },
            {path: 'modBancos', component: modBancos, name: 'manttoBancos' },
            {path: 'modCtasMadre', component: modCtasMadre, name: 'manttoCtasMadre' },
            {path: 'modSubCuentas', component: modSubCuentas, name: 'manttoSubCtas' },
            {path: 'modSucursales', component: modSucursales, name: 'manttoSucursales' },
            {path: 'modReembolsables', component: modReembolsables, name: 'manttoReembolsables' },
            {path: 'modCatProdServ', component: modCatProdServ, name: 'manttoProdServ' }
        ]}
    ]},
    {path: '*', redirect: {name: 'inicio'}}
]