import Vue from 'vue'
import SIAFI from './siafi.vue'
import VueRouter from 'vue-router'
import { rutas } from './rutas'

Vue.use(VueRouter)

const ruteador = new VueRouter({
  mode: 'history',
  routes: rutas
})

export var dataBus = new Vue()
export var eventBus = new Vue()
export var methodsBus = new Vue()
export var eventData = new Vue()

new Vue({
  router: ruteador,
  el: '#siafi',
  render: h => h(SIAFI)
})
